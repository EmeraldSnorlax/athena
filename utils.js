const prefix = '~';
const error = {
    arguments: 'Specify arguments!',
    duplicate: 'Already exists!'

}


module.exports = {
    prefix,
    error
};