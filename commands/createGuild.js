const Discord = require('discord.js');
const fs = require('fs');
const utils = require('../utils');

module.exports = {
	name: 'createguild',
	description: 'Create a guild',
        execute(message, args) {
            if (!args[0]) throw utils.error.arguments;
            let guildName = args[0];
            message.channel.send('Working...');

            fs.readFile('./guilds.json', (err, res) => {
                // check if a guild exists, and throw an error if it does
                if (err) throw err;

                let guilds = JSON.parse(res);
                let guild = guilds.find(el => el.name == guildName);
                console.log(guild);
                if (guild) {
                    message.reply('That guild already exists!')
                } else {
                    // new guild
                    guilds.push({
                        name: guildName,
                        members: [],
                        alliances: [],
                        wars: [],
                    });
                    guilds = JSON.stringify(guilds);
                    fs.writeFileSync('./guilds.json', guilds);
                    message.channel.send('Guild created')
                }
            }
        );
	},
};