const Discord = require('discord.js');
const fs = require('fs');
const utils = require('../utils');

module.exports = {
	name: 'memberinfo',
	description: 'Get info about a member',
        execute(message, args) {
            if (!args[0]) throw utils.error.arguments;
            let userId = args[0].substring(3).slice(0, -1);
            message.channel.send('Working... `(' + userId + ')`');

            fs.readFile('./guilds.json', (err, res) => {
                if (err) throw err;
                let data = JSON.parse(res);

                let userGuild = data.find(el => el.members.includes(userId));
                let embed = new Discord.MessageEmbed()
                    .setTitle('User info for ' + message.author.username)
                    .addField('Guild', userGuild.name, true)
                    .setFooter('Find out more about this guild by doing ' + utils.prefix +'guildinfo ' + userGuild.name);
                message.channel.send(embed);
        });
        

	},
};