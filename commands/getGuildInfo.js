const Discord = require('discord.js');
const fs = require('fs');
const utils = require('../utils');

module.exports = {
	name: 'guildinfo',
	description: 'Get info about a guild',
        execute(message, args) {
            if (!args[0]) throw utils.error.arguments;
            let guildName = args[0];
            message.channel.send('Working...');

            fs.readFile('./guilds.json', (err, res) => {
                if (err) throw err;
                let data = JSON.parse(res);
                let guild = data.find(el => el.name == guildName);

                let memberList = ''
                guild.members.forEach(member => {
                    memberList += '<@' + member + '>' + '\n';
                });

                let allianceList = ''
                guild.alliances.forEach(ally => {
                    allianceList += ally + '\n';
                });

                let warList = ''
                guild.wars.forEach(war => {
                    warList += war + '\n';
                });

                // field values cannot be blank
                if (memberList === '') memberList = 'No one';
                if (allianceList === '') allianceList = 'No allies';
                if (warList === '' ) warList = 'No wars';

                let embed = new Discord.MessageEmbed()
                    .setTitle(guildName)
                    .addFields(
                        { name: 'Members', value: memberList, inline: false},
                        { name: 'Alliances', value: allianceList, inline: true},
                        { name: 'Wars', value: warList, inline: true}
                    )
                message.channel.send(embed);
        });
	},
};